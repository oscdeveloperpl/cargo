<?

include_once('include/app_top.php');

// flush all cache files
Cache::flush();

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>Cargo Test</title>
	<link rel="stylesheet" href="/css/main.css">
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="/js/function.js"></script>
	<script src="/js/main.js"></script>
</head>

<body>

	<div id="main">




		<div id="step0">

			<span class="headerTitle">
				Let's start!
			</span>

			<div class="content">

				<div class="title">Import database</div>

				<div class="ajaxResponse"></div>

				<div class="button">
					<button id="import">Click me!</button>
				</div>

			</div>

		</div>




		<div id="step1">

			<span class="headerTitle">
				Step 1 of 3
			</span>

			<div class="content">

				<div class="title">Add groups</div>

				<div id="groupList">
					<ul></ul>
				</div>

				<div class="ajaxResponse"></div>

				<div style="clear: both;">
					<input type="text" name="groupName" value="" placeholder="Group name..." />
					<span id="groupAdd">+ Add</span>
				</div>

				<div class="button">
					<button id="step1next">Next step...</button>
				</div>

			</div>

		</div>




		<div id="step2">

			<span class="headerTitle">
				Step 2 of 3
			</span>

			<div class="content">

				<div class="title">
					Assign users to groups
					<br>
					<em>First click User than Group</em>
				</div>

				<div class="ajaxResponse"></div>

				<h3>Users</h3>
				<div id="userList">
				</div>

				<h3>Groups</h3>
				<div id="user2groupLists">
					<ol></ol>
				</div>

				<div class="button">
					<button id="step2next">Next step...</button>
				</div>

			</div>

		</div>




		<div id="step3">

			<span class="headerTitle">
				The end!
			</span>

			<div class="content">

				<div class="title">
					Let's see the Social Graph
				</div>

				<div class="ajaxResponse"></div>

				<h3>Choose a user</h3>
				<div id="userList4graph">
				</div>

				<div id="socialGraph">

					<h3 id="userName"></h3>

					<div class="userInfo">
					</div>

					<div style="clear: both; margin: 30px 0 0 0;"></div>

					<div id="userConnection">

						<div id="groups">
							<h3>Groups</h3>
							<ol></ol>
						</div>

						<div id="friends">
							<h3>Friends</h3>
							<ol></ol>
						</div>

						<div id="friendsOfFriends">
							<h3>Friends of friends</h3>
							<ol></ol>
						</div>

						<div id="friendsSuggested">
							<h3>Suggested friends</h3>
							<ol></ol>
						</div>

					</div>

				</div>

			</div>

		</div>




	</div>

</body>
</html>