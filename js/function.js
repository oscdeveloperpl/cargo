/*
 * Import user list to choosen destination
 */
function userListImport ( destination, prefix ) {

	$.ajax({
		url: '/ajax/user.php',
		type: 'POST',
		dataType: 'json',
		data: {
			'action': 'importUserList'
		},
		success: function(data){
			
			$( destination ).append('<ul></ul>');
			
			destination = $( destination ).children('ul');
			
			// k - user id
			// v - object with name and surname
			$.each( data, function( k, v ) {
				$( destination ).append(
					'<li id="' + prefix + k + '">' + v.name + ' ' + v.surname + '</li>'
				);			
			});
			
		}
	});
	
}




/*
 * Shows button Next step
 */
function stepNext ( element, stepNext ) {

	if ( $( element ).length > 0 ) {
		$( stepNext ).fadeIn();
	}

}