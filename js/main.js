$(document).ready(function(){
	
	window.location.hash = '#step0';
	
	// start listen to hash change to show step
	$( window ).on('hashchange', function() {
		
		// base state of div visibility
		$('div[id^="step"]').css({'display':'none'});
		
		
		// take userID from URL
		window.hashMatch = window.location.hash.match( /&userId=[0-9]+/g );
		
		// clean URL to left only step number
		window.location.hash = window.location.hash.replace( /&.*/g, '' );
		
		// start handler for choose user from URL
		if ( window.hashMatch != null && window.location.hash == '#step3' ) {

			window.step3userId = window.hashMatch[0].replace(/[^0-9]+/g, '');
				
			$( '#userGraph' + window.step3userId ).trigger('click');

		}
		// end handler for choose user from URL
		
		// show div with id from URL
		$( window.location.hash )
		.fadeIn();

	});
	// end listen to hash change to show step
	

	
	
	// start step 0 import data 
	$('#import').click(function(){
		
		$.ajax({
			url: '/ajax/db_import.php',
			type: 'POST',
			dataType: 'json',
			beforeSend: function(){
				
				$('#import').fadeOut();
				
				$( window.location.hash + ' .ajaxResponse' )
				.html('Loading...')
				.fadeIn();
				
			},
			error: function(){
				
				$( window.location.hash + ' .ajaxResponse' )
				.html('AJAX error')
				.fadeIn();
				
			},
			success: function(data){

				// start import users list for step 2 and step 3
				userListImport( '#userList', 'user' );
				
				userListImport( '#userList4graph', 'userGraph' );
				// end import users list for step 2 and step 3
				
				
				// load message from import
				$( window.location.hash + ' .ajaxResponse' )
				.html(data.meassage)
				.fadeIn(function(){
					
					// show step 1
					$('#step0')
					.delay(1000)
					.fadeOut(function(){
						
						$('#step1')
						.fadeIn();
						
						window.location.hash = '#step1';
						
					});					
					
				});
								
			}
			
		});
		
	});
	// end step 0 import data

	
	
	
	// start step 1 add group 
	
	/*
	 *  Add a group
	 */
	$('#groupAdd').click(function(){

		$.ajax({
			url: '/ajax/group.php',
			type: 'POST',
			dataType: 'json',
			data: {
				'action': 'add',
				'name': $('input[name="groupName"]').val()				
			},
			beforeSend: function(){
				
				$( window.location.hash + ' .ajaxResponse' )
				.html('Loading...')
				.fadeIn();
				
			},
			error: function(){
				
				$( window.location.hash + ' .ajaxResponse' )
				.html('AJAX error')
				.fadeIn();
				
			},
			success: function( data ) {
				
				if ( data.response == false ) {
					
					// if group exist or inserted name is empty
					$( window.location.hash + ' .ajaxResponse' )
					.html( data.message )
					.delay( 1000 )
					.fadeOut(function(){
						$(this).html('');
					});
					
				} else {
					
					$( window.location.hash + ' .ajaxResponse' )
					.fadeOut( 100, function(){
						
						$(this).html('');
						
						// show inserted group name
						$('#groupList ul')
						.delay( 1000 )
						.append('<li>' + data.name + '</li>');						
						
						// show button to next step
						stepNext( '#groupList ul li', '#step1next' );
						
					});
					
					// insert new group for step 3 
					$('#user2groupLists')
					.append(
						'<div id="group' + data.id + '"><h3>' + data.name + '</h3><ol></ol></div>'
					);
								
				}
				
				// reset group name input
				$('input[name="groupName"]').val('');

			}
			
		});
		
	});
	
	
	// start Bind click event for Enter key press 
	$('input[name="groupName"]').keypress(function(e) {

		if ( e.which == 13 ) {
			
			// trigger add a group
			$('#groupAdd').trigger('click');
		
		}
				
	});
	// end Bind click event for Enter key press 

	
	
	
	// start go to step 2
	$('#step1next').click(function(){
		
		// show step 2		
		$('#step1').fadeOut(function(){
			$('#step2').fadeIn();
		});
		
		window.location.hash = '#step2';
		
	});
	// end go to step 2
	
	// end step 1 add group
	
	

	
	// start step 2 relate user to group
	$('#userList').on('click', 'li', function(){ // elements are loaded by ajax dynamically, it needed to use ON function
		
		// check if user has been clicked/active
		if ( $.isEmptyObject(window.user) == false && $(this).css('background-color') != 'transparent' ) {
			
			// clear background of active user
			$(this).css({'background':'none'});
			
			// clear memory, delete object which handles the user
			delete( window.user );
			
			// change background for div with pointed group (compatibility)
			$('#user2groupLists div').hover(
				function(){
					$(this).css({'cursor':'default','background':'none'});		
				},
				function(){
					$(this).css({'cursor':'default','background':'none'});		
				}
			);

		} else {

			window.user = new Object();
			
			// get user id from ID property of clicked li element
			window.user.id = $(this).prop('id').replace(/[^0-9]/g, '');
			
			window.user.name = $(this).text();

			// clear all li elements background
			$('#userList li').css({'background':'none'});
		
			// set li background when is active after click
			$(this).css({'background':'#ededed'});
			
			// change background for div with pointed group
			$('#user2groupLists div').hover(
				function(){
					$(this).css({'cursor':'pointer','background':'#ededed'});		
				},
				function(){
					$(this).css({'cursor':'default','background':'none'});		
				}
			);
		
		}
	
	}); 

	
	 // elements are loaded by ajax dynamically, it needed to use ON function
	$('#user2groupLists').on('click', 'div', function(){
		
		// active when user was clicked first
		if ( $.isEmptyObject(window.user) == false ) {

			window.group = new Object();
			
			// get group id from ID property of clicked div element
			window.group.id = $(this).prop('id').replace(/[^0-9]/g, '');
			
			window.group.name = $(this).text();

			// clear all div elements background
			$('#user2groupLists div').css({'background':'none'});
			
			if ( $(this).is(':contains("' + window.user.name + '")') == false ) {
							
				$.ajax({
					url: '/ajax/user.php',
					type: 'POST',
					dataType: 'json',
					data: {
						'action': 'user2group',
						'userName': window.user.name,
						'userId': window.user.id,
						'groupId': window.group.id
					},
					beforeSend: function(){
						
						$( window.location.hash + ' .ajaxResponse' )
						.html('Loading...')
						.fadeIn();
						
					},
					error: function(){
						
						$( window.location.hash + ' .ajaxResponse' )
						.html('AJAX error')
						.fadeIn();
						
					},
					success: function ( data ) {

						if ( data.response == true ) {
							
							$( window.location.hash + ' .ajaxResponse' )
							.fadeOut(function(){
								
								$(this).html('');
								
								$( '#group' + data.groupId )
								.css({'cursor':'default'})
								.children('ol').append(
									'<li>' + data.userName + '</li>'
								);
								
								// show button to next step
								stepNext( '#group' + data.groupId + ' li', '#step2next' );
								
							});
							
						} else {
							
							$( window.location.hash + ' .ajaxResponse' )
							.html( data.message )
							.delay( 1000 )
							.fadeOut(function(){
								$(this).html('');
							});
							
						}
						
					}
					
				});				
				
			} else {
				
				$( window.location.hash + ' .ajaxResponse' )
				.html( 'User has been already assigned to this group' )
				.fadeIn()
				.delay( 1000 )
				.fadeOut(function(){
					$(this).html('');
				});
				
			}
			
		}
		
		delete( window.group );
		
	});
	
	
	
	
	// start go to step 3
	$('#step2next').click(function(){
		
		// show step 3		
		$('#step2').fadeOut(function(){
			$('#step3').fadeIn();
		});
		
		window.location.hash = '#step3';
		
	});
	// end go to step 3
	
	// end step 2 relate user to group

	
	
	
	// start the last step 3
	
	// handler to choose a user. User list is loaded by ajax dynamically
	$('#userList4graph').on('click', 'li[id^="userGraph"]', function(){
		
		
		//console.log(window.step3userId);
		if ( window.step3userId == null ) {
			
			window.step3userId = $(this).prop('id').replace(/[^0-9]/g, ''); 

		}

		$.ajax({
			url: '/ajax/user.php',
			type: 'POST',
			dataType: 'json',
			data: {
				'action': 'userGraph',
				'userId': window.step3userId
			},
			beforeSend: function(){
				
				$( window.location.hash + ' .ajaxResponse' )
				.html('Loading...')
				.fadeIn();
				
			},
			error: function(){
				
				$( window.location.hash + ' .ajaxResponse' )
				.html('AJAX error')
				.fadeIn();
				
			},
			success: function ( data ) {

				if ( data.response == true ) {
					
					$( window.location.hash + ' .ajaxResponse' )
					.fadeOut(function(){
						
						$(this).html('');
						
						$( '#socialGraph, #userConnection' )
						.fadeIn(function(){
							
							
							// show name of choosen user
							$('#userName').html(
								data.user.name + ' ' + data.user.surname
							);
							
							
							// show user's informations
							$(this).find( '.userInfo' ).html(
								'Gender: ' + data.user.gender + '<br />' +
								'Birth date: ' + data.user.dob + '<br />' +
								'Age: ' + data.user.age	
							);
							

							// show user's friends
							$('#groups ol').html(''); // clean content
							
							$.each( data.groups, function( k, v ) {
								
								$('#groups ol').append(
									'<li>' + v.name + '</li>'
								);
								
							});
							
							
							// show user's friends
							$('#friends ol').html(''); // clean content
							
							$.each( data.friends, function( k, v ) {
								
								$('#friends ol').append(
									'<li>' + v.name + ' ' + v.surname + '</li>'
								);
								
							});
							
							
							// show user's friends of friends
							$('#friendsOfFriends ol').html(''); // clean content
							
							$.each( data.friendsOfFriends, function( k, v ) {
								
								$('#friendsOfFriends ol').append(
									'<li>' + v + '</li>'
								);
								
							});							

							
							// show user's friends of friends
							$('#friendsSuggested ol').html(''); // clean content
							
							$.each( data.friendsSuggested, function( k, v ) {
								
								$('#friendsSuggested ol').append(
									'<li>' + v + '</li>'
								);
								
							});	
							
						});
						
					});
					
				} else {
					
					$( window.location.hash + ' .ajaxResponse' )
					.html( data.message )
					.delay( 1000 )
					.fadeOut(function(){
						$(this).html('');
					});
					
				}
				
			}
			
		});
		
		delete( window.step3userId ); // reset userId from url
		
	});
	
	// end the last step 3
	
	
	
});