<?

// start load system configuration
header ( 'Content-Type: text/html; charset=utf-8' );

include_once ( $_SERVER ['DOCUMENT_ROOT'] . '/include/config.php' );

set_error_handler ( 'errorHandler', SYSTEM_DEBUG );

error_reporting ( SYSTEM_DEBUG );

include_once DIR_FUNC . 'function.php';

spl_autoload_register ( 'classLoader' );

set_exception_handler ( 'exeptionHandler' );
// end load system configuration

// database connection
$db = DB::getInstance();

?>