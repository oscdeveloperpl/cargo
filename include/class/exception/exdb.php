<?

/**
 * Database exeption class
 *
 */
class ExDB extends Exception {




	/**
	 * Location where exception was thrown
	 *
	 * @var string $location
	 */
	private $location;


	/**
	 * Object with error
	 *
	 * @var object $error
	 */
	private $error;




	/**
	 * Class constructor
	 *
	 * @param string $location Location where exception was thrown
	 * @param PDOException $error Error object
	 */
	public function __construct ( $location, PDOException $error = null ) {

		$this->location = $location;
		$this->error = $error;

		$this->showError();

	}




	/**
	 * Show error as a string
	 *
	 */
	private function showError () {

		echo '
		<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<style>
			body {
				font-family: Calibri;
				font-size: 14px;
				line-height: 25px;
			}
			</style>
		</head>
		<body>
			<h1>Database Error</h1>
			<b>Message:</b> ' . $this->error->message . '
			<br><b>Location:</b> ' . $this->location . '
			<br><b>Line:</b> ' . $this->getLine () . '
			<br><b>File:</b> ' . $this->getFile () ./* '
			<br><b>Trace:</b><br>' . str_replace ( "\n", '<br>', $this->getTraceAsString () ) . */'
			<br>
			<hr>
			<br><br>
		</body>
		</html>
		';

		die;

	}


}

?>