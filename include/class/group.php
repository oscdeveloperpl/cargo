<?

/**
 * Groups administration
 */
class Group {




	/**
	 * Database handler
	 * @var object
	 */
	private $db;




  /**
   * Get data to import
   * @param array $data
   */
  public function __construct () {

  	$this->db = DB::getInstance();

  }




  /**
   *  Trigger for private functions, ex. add
   *  @params array Array with post data
   *  @return undefined
   */
  public function action ( $array ) {

  	$action = $array['action'];

  	return $this->$action( $array );

  }




  /**
   *  Add group to database
   *  @params array Array with post data
   *  @return mixed Return formatted json
   */
  private function add ( $array ) {

  	$name = $array['name'];

  	if ( APP::isNull( $name ) ) {

  		// format json response for ajax
  		$return = array(
  			'response' => false,
  			'message' => 'Give a name of group'
  		);

  		return APP::data2json( $return );

  	}


  	// check if group with given name exist
  	$this->db->qBind("
			SELECT
				COUNT(*) AS count

			FROM
				groups

  		WHERE

  			name = :name

  		LIMIT 1
			",
	  	array (
				':name' => $name
			)
  	);

  	// group exist
  	if ( $this->db->f('count') > 0 ) {

  		// format json response for ajax
  		$return = array(
  			'response' => false,
  			'message' => 'Group <em>' . $name . '</em> already exists'
  		);

  		return APP::data2json( $return );

  	}

  	// group doesn't exist
		$this->db->qBind("
			INSERT INTO
			groups

			(
				id,
				name
			)

			VALUES

			(
				DEFAULT,
				:name
			)
			",
			array (
				':name' => $name
			)
		);

		// format json response
		$return = array(
			'response' => true,
			'id' => $this->db->lastInsertId(),
			'name' => $name
		);

  	return APP::data2json( $return );

  }




}

?>