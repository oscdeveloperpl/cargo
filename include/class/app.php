<?

/**
 * General application functions
 */
class App {




	/**
	 *  Return data in json format
	 *  @params mixed $data
	 *  @return array
	 */
	public static function data2json ( $data ) {

		return json_encode( $data );

	}




	/**
	 *  Convert json to array
	 *  @params mixed $data
	 *  @return array
	 */
	public static function json2data ( $data ) {

		return json_decode( $data );

	}




	/**
	 * Parse incoming POST data
	 * @param string $key
	 * @return mixed
	 */
	public static function inPost ( $key = null ) {

		if ( self::isNull( $key ) ) {

			$post = $_POST;

		} else {

			$post = $_POST[ $key ];

		}

		return $post;

	}




	/**
	 * Parse incoming GET data
	 * @param string $key
	 * @return mixed
	 */
	public static function inGet ( $key = null ) {

		if ( self::isNull( $key ) ) {

			$get = $_GET;

		} else {

			$get = $_GET[ $key ];

		}

		return $get;

	}




  /**
   * Lefts only numbers
   *
   * @param string $value
   * @return number
   */
  public static function toNum ( $value ) {

    return preg_replace ( '#[^0-9]#', '', $value );

  }




  /**
   * Lefts only letters
   *
   * @param string $value
   * @return string
   */
  public static function toLtr ( $value ) {

    return preg_replace ( '#[^a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹĆŃ]#', '', $value );

  }




  /**
   * Lefts only letters and numbers
   *
   * @param string $value
   * @return string
   */
  public static function toNumLtr ( $value ) {

    return preg_replace ( '#[^0-9a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹĆŃ]#', '', $value );

  }




  /**
   * Lefts only base letters and sign _
   *
   * @param string $value
   * @return string
   */
  public static function toLtrUscr ( $value ) {

  	return preg_replace ( '#[^a-zA-Z_0-9]#', '', $value );

  }




  /**
   * Check if given value is not empty
   *
   * @param mixed $value
   * @return boolean If not empty, returns true
   */
  public static function isNotNull ( $value = '' ) {

  	if ( is_array ( $value ) && count ( $value ) == 0 ) {
  		return false;
  	}


  	if ( is_object ( $value ) && !empty ( $value ) ) {

  		if ( empty ( $value ) ) {
  			return false;
  		} else {
  			return true;
  		}

  	}


  	if ( is_string ( $value ) ) {
  		$value = str_replace ( ' ', '', $value );
  	}


  	if ( is_bool ( $value ) ) {
  		$value = null;
  	}


  	switch ( $value ) {
  		case is_null ( $value ):
  		case 'unknown':
  		case 'undefined':
  		case 'null':
  		case '':
  		case NULL:
  			return false;
  	}

  	return true;

  }




  /**
   * Check if given value is empty
   *
   * @param $value mixed
   * @return boolean Returns true if $value is empty
   */
  public static function isNull ( $value = '' ) {

  	if ( is_object ( $value ) ) {
  		return empty ( $value );
  	}

  	if ( is_string ( $value ) ) {
  		$value = str_replace ( ' ', '', $value );
  	}

  	if ( is_bool ( $value ) ) {
  		$value = null;
  	}

  	if ( is_array ( $value ) && count ( $value ) == 0 ) {
  		return true;
  	}

  	switch ( $value ) {
  		case is_null ( $value ):
  		case 'unknown':
  		case 'undefined':
  		case 'null':
  		case '':
  		case NULL:
  			return true;
  	}

  	return false;

  }




}

?>