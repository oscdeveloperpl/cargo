<?

/**
 * Users administration
 *
 */
class User {




	/**
	 * Database handler
	 * @var object
	 */
	private $db;




  /**
   * Get data to import
   * @param array $data
   */
  public function __construct () {

  	$this->db = DB::getInstance();

  }




  /**
   *  Trigger for private functions, ex. user2group with passing parameters
   *  @params array Array with post data
   *  @return undefined
   */
  public function action ( $array ) {

  	$action = $array['action'];

  	return $this->$action( $array );

  }




  /**
   * Convert age to date of birth (year only)
   * @param number $age
   * @return string
   */
  public static function dobFormat ( $age ) {

  	if ( APP::isNotNull ( $age ) ) {

  		$dob = ( date('Y') - $age ) . '-00-00'; // lack of informations about month and day

  	} else {

  		$dob = NULL; // in case of lack of information about age

  	}

  	return $dob;

  }




  /**
   * Convert date of birth to age
   * @param number $age
   * @return string
   */
  public static function ageFormat ( $dob ) {

  	if ( APP::isNull ( $dob ) ) {

  		$age = 'not avaiable';

  	} else {

  		$age = date('Y') - substr( $dob, 0, 4 );

  	}

  	return $age;

  }




  /**
   * Convert string, ex. male = 1, female = 2, undefined = 0
   * @param string $gender
   * @return number
   */
  public static function genderMapping ( $gender = NULL ) {

  	switch ( $gender ) {

  		case 'male':
  			$gender = 1;
  			break;

  		case 'female':
  			$gender = 2;
  			break;

  		default:
  			$gender = 0;

  	}

  	return $gender;

  }




  /**
   * Convert number of sex to string format, ex. male = 1, female = 2, undefined = 0
   * @param integer $gender
   * @return number
   */
  public static function genderFormat ( $gender ) {

  	if ( APP::isNull( APP::toNum( $gender ) ) ) {

  		throw new Ex( 'genderFormat', 'The given gender is not correct' );

  	}

  	switch ( $gender ) {

  		case 1:
  			$gender = 'male';
  			break;

  		case 2:
  			$gender = 'female';
  			break;

  		default:
  			$gender = 'undefined';

  	}

  	return $gender;

  }




  /**
   *  Assign user to group
   *  @params array
   *  @return mixed Return formatted json
   */
  private function user2group ( $array ) {

  	// get user2group list
  	$this->db->qBind("
			SELECT
				COUNT(*) AS count

			FROM
				user2group

  		WHERE

  			user_id = :user_id
  			AND
  			group_id = :group_id

  		LIMIT 1
		",
  	array (
  		'user_id' => $array['userId'],
  		'group_id' => $array['groupId']
  	));

  	// check if assignment exist
  	if ( $this->db->f('count') > 0 ) {

  		$return = array(
  			'response' => false,
  			'message' => 'User has been already assigned to this group'
  		);

  		return APP::data2json( $return );

  	}

  	// assignment doesn't exist
		$this->db->qBind("
			INSERT INTO
			user2group

			(
				user_id,
				group_id
			)

			VALUES

			(
				:user_id,
				:group_id
			)
			",
			array (
	  		'user_id' => $array['userId'],
	  		'group_id' => $array['groupId']
			)
		);

		// format json response
		$return = array(
			'response' => true,
			'userName' => $array['userName'],
			'groupId' => $array['groupId'],
			'message' => 'User has been assigned to group'
		);

  	return APP::data2json( $return );

  }




  /**
   *  Import users list
   *  @params array
   *  @return mixed Return formatted json
   */
 	private function importUserList ( $array = null ) {

		if ( Cache::check( $array['action'] ) ) {

			// format output
			$return = APP::json2data( Cache::get( $array['action'] ) );

		} else {

			// get users list
	  	$this->db->q("
				SELECT
					id,
	  			name,
	  			surname

				FROM
					users
			");

	  	$userList = $this->db->fAll();

	  	// output users list
	  	$user = array();

	  	// prepare users list for json output
			foreach ( $userList as $v ) {

				$user[ $v['id'] ] = array(
					'name' => $v['name'],
					'surname' => $v['surname']
				);

			}

			Cache::set( $array['action'], APP::data2json( $user ) );

	  	$return = $user;

		}

		return APP::data2json( $return );

  }




  /**
   *  Show user connections
   *  @params array $array
   *  @return mixed Return formatted json
   */
  public function userGraph ( $array ) {

  	if ( APP::isNull( APP::toNum( $array['userId'] ) ) ) {

  		throw new Ex( 'userGraph', 'The given userId is not correct' );

  	}


  	$userId = (int)$array['userId'];

  	$user = $this->userInfo( $userId );

  	$groups = $this->userGroups( $userId );

  	$friends = $this->friends( $userId );

  	$friendsOfFriends = $this->friendsOfFriends( $userId );

  	$friendsSuggested = $this->friendsSuggested( $userId );

  	$return = array(
  		'response' => true,
  		'user' => $user,
  		'groups' => $groups,
  		'friends' => $friends,
  		'friendsOfFriends' => $friendsOfFriends,
  		'friendsSuggested' => $friendsSuggested
  	);

  	// format json response
  	return APP::data2json( $return );

  }




  /**
   *  Show user informations
   *  @params integer $userId
   *  @return array Return list of user informations as an array
   */
  private function userInfo ( $userId ) {

  	if ( Cache::check( 'userInfo' . $userId ) ) {

  		$return = APP::json2data( Cache::get( 'userInfo' . $userId ) );

  	} else {

	  	$this->db->qBind("
				SELECT
					*

				FROM
					users

	  		WHERE
	  			id = :id

	  		LIMIT 1
			",
	  	array (
	  		'id' => $userId
	  	));

	  	$user = $this->db->f();

	  	if ( APP::isNull( $user['dob'] ) ) {

	  		$user['dob'] = 'not available';
	  		$dob = '';

	  	} else {

	  		$user['dob'] = substr( $user['dob'], 0, 4 );
	  		$dob = $user['dob'];

	  	}

	  	$return = array(
	  		'id' => $user['id'],
	  		'name' => $user['name'],
	  		'surname' => $user['surname'],
	  		'dob' => $user['dob'],
	  		'age' => self::ageFormat( $dob ),
	  		'gender' => self::genderFormat( $user['gender'] )
	  	);

	  	// put single user info to cache
	  	Cache::set( 'userInfo' . $user['id'], APP::data2json( $return ) );

  	}

  	return $return;

  }




  /**
   *  Show user's groups informations
   *  @params integer $userId
   *  @return array Return list of groups connected to user as an array
   */
  private function userGroups ( $userId ) {

  	$this->db->qBind("
  		SELECT
  			g.id,
        g.name
				
  		FROM
  			groups g
				
				INNER JOIN
					user2group u2g
					ON
						g.id = u2g.group_id
					AND
						u2g.user_id = :user_id	
			",
  		array (
  			'user_id' => $userId
  		));

  	$return = $this->db->fAll();

  	return $return;

  }




  /**
   *  Show user friends
   *  @params integer $userId
   *  @return array Return list of friends as an array
   */
  private function friends ( $userId ) {

  	$this->db->qBind("
			SELECT
				id,
  			name,
  			surname
				
			FROM
				users u
				
        INNER JOIN
					user2friend u2f
					ON
						u.id = u2f.user_friend_id
          AND
						u2f.user_id = :user_id
		",
  	array (
  		'user_id' => $userId
  	));

  	$return = $this->db->fAll();

  	return $return;

  }




  /**
   *  Show user's friends of friends
   *  @params integer $userId
   *  @return array Return list of user informations as an array
   */
  private function friendsOfFriends ( $userId ) {

  	$this->db->qBind("
			SELECT DISTINCT
    		friend.user_friend_id AS id

			FROM
  			user2friend user

			INNER JOIN
  			user2friend friend
  			ON friend.user_id = user.user_friend_id

			WHERE

  			user.user_id = :user_id
  			AND

  			friend.user_friend_id != user.user_id
  			AND

  			friend.user_friend_id NOT IN (

  				SELECT
  					notin.user_friend_id

        	FROM
  					user2friend notin

        	WHERE
  					notin.user_id = user.user_id

        )
		",
  	array (
  		'user_id' => $userId
  	));

  	$friendsOfFriends = $this->db->fAll();

  	// get names of users from cache
  	$userList = APP::json2data( Cache::get( 'importUserList' ) );

  	$return = array();

  	foreach ( $friendsOfFriends as $v ) {

  		$return[] = $userList->$v['id']->name . ' ' . $userList->$v['id']->surname;

  	}

  	return $return;

  }




  /**
   *  Show suggested friends to user
   *  @params integer $userId
   *  @return array Return list of user suggested friends as an array
   */
  private function friendsSuggested ( $userId ) {

  	// create view for mutual group
  	$this->db->q("DROP VIEW IF EXISTS mutual_group");

  	$this->db->qBind("
  		CREATE VIEW
  			mutual_group

  		AS
				SELECT
					DISTINCT user_id

				FROM
					user2group

				WHERE
					user_id != :user_id

					AND
					group_id IN (

						SELECT
							group_id

						FROM
							user2group

						WHERE
							user_id = :user_id

					)
  		",
	  	array (
	  		'user_id' => $userId
	  	));


  	// create view for mutual friends
  	$this->db->q("DROP VIEW IF EXISTS mutual_friend");

  	$this->db->qBind("
  		CREATE VIEW
  			mutual_friend

  		AS
				SELECT
	  			user_id

	  		FROM
	  			user2friend

	  		WHERE
					user_friend_id IN (

	    			SELECT
	  					user_friend_id
	    			FROM
	    				user2friend
	   				WHERE
	    				user_id = :user_id

	    		)

	  			AND
					user_id NOT IN (

						SELECT
	  					user_id
	    			FROM
	    				user2friend
	    			WHERE
	    				user_friend_id = :user_id

	    		)

	  			AND
					user_id != :user_id

	  			GROUP BY user_id
	 				HAVING COUNT(user_id) >=2
  	",
  	array (
  		'user_id' => $userId
  	));


  	// execute select based on views
  	$this->db->q("
  		SELECT
  			a.user_id AS id

  		FROM
  			mutual_group AS a

  		INNER JOIN
  			mutual_friend AS b
	  		ON
				a.user_id = b.user_id
		");

  	$friendsSuggested = $this->db->fAll();

  	// get names of users from cache
  	$userList = APP::json2data( Cache::get( 'importUserList' ) );

  	$return = array();

  	foreach ( $friendsSuggested as $v ) {

  		$return[] = $userList->$v['id']->name . ' ' . $userList->$v['id']->surname;

  	}

  	return $return;

  }


}

?>