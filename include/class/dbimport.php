<?

/**
 * Import to database data from data.php file
 *
 */
class DBImport {




	/**
	 * Database handler
	 * @var object
	 */
	private $db;

	/**
	 * Data to import
	 * @var string
	 */
	private $data;




  /**
   * Get data to import
   * @param array $data
   */
  public function __construct ( $data ) {

  	$this->db = DB::getInstance();

  	$this->data = $data;

  }




	/**
	 *  Checks if import has been already done
	 */
	public function import () {

		$this->db->q("
			SELECT
				COUNT(*) AS count

			FROM
				users
		");

		if ( $this->db->f('count') > 0 ) {
			
			$this->db->q("SET FOREIGN_KEY_CHECKS = 0");
			
			$this->db->truncate('user2friend');

			$this->db->truncate('user2group');

			$this->db->truncate('users');

			$this->db->truncate('groups');
			
			$this->db->q("SET FOREIGN_KEY_CHECKS = 1");

		}

		return self::importExecute();

	}




  /**
   * Execute import of given data
   * @return boolean
   */
  private function importExecute () {

  	// start insert into user table
  	foreach ( $this->data as $v ) {

  		$v['age'] = User::dobFormat( $v['age'] );

  		$v['gender'] = User::genderMapping( $v['gender'] );

  		$this->db->qBind("
        INSERT INTO
          users

          (
  					id,
  					name,
  					surname,
  					dob,
  					gender
					)

        VALUES

        (
          :id,
          :name,
          :surname,
          :dob,
          :gender
        )
        ",
  			array (
  				':id' => $v['id'],
					':name' => $v['firstName'],
					':surname' => $v['surname'],
					':dob' => $v['age'], // IDEA: age as full date format, not simple age number
  				':gender' => $v['gender']
				)
  		);

  	}
  	// end insert into user table




  	// start insert into user2friend table
  	// second foreach because of foreign keys integrity with user table, user_id<=>id column
  	foreach ( $this->data as $v ) {

	  	foreach ( $v['friends'] as $friend ) {

	  		$this->db->qBind("
	        INSERT INTO
	          user2friend

	          (
	  					user_id,
	  					user_friend_id
						)

	        VALUES

	        (
	          :user_id,
	          :user_friend_id
	        )
	        ",
  				array (
  					':user_id' => $v['id'],
  					':user_friend_id' => $friend
  				)
	  		);

	  	}

  	}
  	// end insert into user2friend table

  	// format json response
  	$return = array(
  		'response' => true,
  		'message' => 'Data imported successfully'
  	);

  	return APP::data2json( $return );

  }


}

?>