<?

/**
 * Cache administration
 *
 * All cache is based on files.
 * Filenames are hashed to safety.
 * In this version only user list and single user info is cached.
 */
class Cache {




	/**
	 *  Check if cache exists
	 *  @params string $name Name of file cache
	 *  @return mixed
	 */
	public static function check ( $name ) {

		return file_exists( DIR_CACHE . sha1( $name ) );

	}




	/**
	 *  Get cached file
	 *  @params string $name Name of file cache
	 *  @return mixed
	 */
	public static function get ( $name ) {

		$name = sha1( $name );

		if ( file_exists( DIR_CACHE . $name ) ) {

			return file_get_contents( DIR_CACHE . $name );

		}

		// if no cache
		return false;

	}




	/**
	 *  Build cache file.
	 *  @params string $name Name of file cache
	 *  @params boolean
	 */
	public static function set ( $name, $data ) {

		try {

			$file = DIR_CACHE . sha1( $name );

			$fhandler = fopen( $file, 'w' );
			flock( $fhandler,LOCK_EX );
			fwrite( $fhandler, $data );
			flock( $fhandler, LOCK_UN );
			fclose( $fhandler );

			return true;

		} catch ( Ex $e ) {

			throw new Ex( 'Cache set', 'Write a file' );

		}

	}




	/**
	 *  Flush cached file
	 *  @params mixed $name Name of file cache. If null flush all files from cache dir
	 *  @return boolean
	 */
	public static function flush ( $name = null ) {

		if ( APP::isNotNull( $name ) ) {

			return unlink( DIR_CACHE . sha1( $name ) );

		} else {

			return array_map('unlink', glob( DIR_CACHE . '*' ) );

		}

	}



}
