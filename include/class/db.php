<?

/**
 * Extension for database PDO class
 * Singleton
 *
 */
class DB extends PDO {




	/**
	 * DB instance
	 *
	 * @var unknown
	 */
	private static $_instance = null;


	/**
	 * DB object
	 *
	 * @var unknown
	 */
	private $db;


	/**
	 * DB statement
	 *
	 * @var unknown
	 */
	private $stmt;


	/**
	 * Number of executed queries
	 *
	 * @var integer
	 */
	private $qCount = 0;




	/**
	 * Class constructor
	 *
	 * @param string $host
	 * @param string $dbname
	 * @param string $user
	 * @param string $pass
	 * @throws ExDB
	 */
	public function __construct ( $host, $dbname, $user, $pass ) {

		// options for PDO initialization
		$options = array (
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
			PDO::ATTR_PERSISTENT => false,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);

		try {

			$this->db = new parent ( 'mysql:host=' . $host . ';dbname=' . $dbname . ';', $user, $pass, $options );

		} catch ( PDOException $e ) {

			throw new ExDB (
				'Constructor',
				$e
			);

		}

	}




	/**
	 * Instance for singleton
	 *
	 * @return object
	 */
	public static function getInstance() {

		if ( empty ( self::$_instance ) ) {

			self::$_instance = new self ( DB_HOST, DB_NAME, DB_USER, DB_PASS );

		}

		return self::$_instance;

	}




	/**
	 * Preparation of single query (not safe, without binding)
	 *
	 * @param string $query
	 * @throws ExDB
	 */
	public function q ( $query, $execute = true ) {

		try {

			$this->stmt = $this->db->prepare ( $query );

			if ( $this->stmt === false ) {

				throw new ExDB (
					'q function',
					$query
				);

			} else if ( $execute === true ) {

				$this->execute();

			}

		} catch ( PDOException $e ) {

			throw new ExDB (
				'Query function',
				$e
			);

		}

	}




	/**
	* Query with binding parameters
	*
	* @param string $query Query
	* @param array $params Parameters with values to bind
	* @param boolean $execute Execute after binding?
	*/
	public function qBind ( $query, $params, $execute = true ) {

		$this->q ( $query, false );

		foreach ( $params as $k => $v ) {

			$this->bind ( $k, $v );

		}

		if ( $execute ) $this->execute();

	}




	/**
	 * Binding parameters to query
	 *
	 * @param unknown $key
	 * @param unknown $value
	 * @param string $type
	 * @throws ExDB
	 */
	public function bind ( $key, $value, $type = null ) {

		if ( is_null ( $type ) ) {

			switch ( true ) {

				case is_int ( $value ):
					$type = PDO::PARAM_INT;
				break;

				case is_bool ( $value ):
					$type = PDO::PARAM_BOOL;
				break;

				case is_null ( $value ):
					$type = PDO::PARAM_NULL;
				break;

				default:
				$type = PDO::PARAM_STR;

			}

		}

		try {

			$this->stmt->bindValue ( $key, $value, $type );

		} catch ( PDOException $e ) {

			throw new ExDB (
				'Bind function',
				$e
			);

		}

	}




	/* (non-PHPdoc)
	 * @see PDO::prepare()
	 */
	public function prepare ( $query ) {

		try {

			return $this->db->prepare ( $query );

		} catch ( PDOException $e ) {

			throw new ExDB (
				'Prepare function',
				$e
			);

		}

	}




	/**
	 * Execute prepared before statement
	 * @throws ExDB
	 */
	public function execute() {

		$this->qCount++;

		try {

			return $this->stmt->execute();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'Execute function',
				$e
			);

		}

	}




	/**
	 * Fetch all rows
	 * @throws ExDB
	 */
	public function fAll() {

		try {

			return $this->stmt->fetchAll();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'fetchAll function',
				$e
			);

		}

	}




	/**
	 * Fetch single row
	 * @param string $column Name of returned single column
	 * @throws ExDB
	 * @return unknown
	 */
	public function f ( $column = null ) {

		try {

			if ( APP::isNull( $column ) ) {

				return $this->stmt->fetch();

			} else {

				$f = $this->stmt->fetch();

				return $f[$column];

			}

		} catch ( PDOException $e ) {

			throw new ExDB (
				'Fetch function',
				$e
			);

		}

	}




	/* (non-PHPdoc)
	 * @see PDO::lastInsertId()
	 */
	public function lastInsertId() {

		try {

			return $this->db->lastInsertId();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'lastInsertId function',
				$e
			);

		}

	}




	/**
	 * Return of auto increment value of given table
	 * @param string $table
	 * @throws ExDB
	 * @return number
	 */
	public function getAi ( $table ) {

		try {

			$this->stmt = $this->db->query("SHOW TABLE STATUS LIKE '" . $table . "'");

		} catch ( PDOException $e ) {

			throw new ExDB (
				'getAi function',
				$e
			);

		}

		$getAi = $this->stmt->fetch();

		return $getAi['Auto_increment'];

	}




	/**
	 * Explained query for optimalization
	 * @param string $query
	 * @throws ExDB
	 */
	public function explain ( $query ) {

		try {

			$this->stmt = $this->db->query ( "EXPLAIN EXTENDED " . $query . "" );

		} catch ( PDOException $e ) {

			throw new ExDB (
				'Explain function',
				$e
			);

		}

		return $this->stmt->fetchAll();

	}




	/* (non-PHPdoc)
	 * @see PDO::beginTransaction()
	 */
	public function beginTransaction() {

		try {

			return $this->db->beginTransaction();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'beginTransaction function',
				$e
			);

		}

	}

	/**
	 * Ends transaction via commit
	 * @throws ExDB
	 */
	public function endTransaction() {

		try {

			return $this->db->commit();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'endTransaction function',
				$e
			);

		}

	}




	/**
	 * Check whether transation started
	 * @throws ExDB
	 * @return boolean
	 */
	public function isTransaction() {

		try {

			return ( boolean ) $this->inTransaction();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'isTransaction function',
				$e
			);

		}

	}




	/**
	 * Cancel started transaction
	 * @throws ExDB
	 */
	public function cancelTransaction() {

		try {

			return $this->db->rollBack();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'cancelTransaction function',
					$e
			);

		}

	}




	/**
	 * Count rows
	 * @throws ExDB
	 */
	public function rowCount() {

		try {

			return $this->stmt->rowCount();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'rowCount',
					$e
			);

		}

	}




	/**
	 * Return amount of executed queries
	 * @return number
	 */
	public function qCount() {

		return $this->qCount;

	}




	/**
	 * Truncate the table
	 * @param string $table
	 */
	public function truncate ( $table ) {
		$this->q("
			TRUNCATE `" . APP::toLtrUscr( $table ) . "`
		");
	}




	/**
	 * Return statement with parameters. Echo output string.
	 * @return string
	 */
	public function debug() {

		try {

			$this->stmt->debugDumpParams();

		} catch ( PDOException $e ) {

			throw new ExDB (
				'debugDumpParams function',
					$e
			);

		}

	}




}

?>