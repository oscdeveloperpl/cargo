<?

/**
 * Print_r
 * @param mixed $value
 */
function pr ( $value ) {

  echo '<pre>' . "\n";

  if ( is_array ( $value ) ) {

    print_r ( $value );

  } else {

    var_dump ( $value );

	}

	echo "\n" . '</pre>';

}




/**
 * Print_r + die
 *
 * @param mixed $value
 */
function pd ( $value ) {

  echo '<pre>' . "\n";

  if ( is_array ( $value ) ) {

    print_r ( $value );

  } else {

    var_dump ( $value );

  }

  echo "\n" . '</pre>';

  die ();

}




/**
 * Automatyczne ładowanie klas
 *
 * @param string $class Nazwa klasy
 */
function classLoader( $class ) {

  $class = strtolower ( $class );

  if ( stripos ( $class, 'ex' ) !== false ) {

    $dir = DIR_EX;

  } else {

    $dir = DIR_CLASS;

  }

  $class = $dir . $class;

  include_once($class . '.php');

}




/**
 * Default exeption handler
 * @param unknown $exception
 */
function exeptionHandler ( $exception ) {

  echo '
  <html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style>
  body {
  	font-family: Verdana, Arial;
  	font-size: 12px;
    line-height: 25px;
  }
  </style>
  </head>
  <body>
  <b>Error</b>
  <br><b>Code:</b> ' . $exception->getCode () ./* '
  <br><b>Location:</b> ' . $text . */ '
  <br><b>Line:</b> ' . $exception->getLine () . '
  <br><b>File:</b> ' . $exception->getFile () . '
  <br><b>Trace:</b><br>' . str_replace ( "\n", '<br>', $exception->getTraceAsString () ) . '
  <br>
  ============
  <br>
  </body>
  </html>
  ';

  die;

}

/**
 * Funkcja do przechwytywania błędów interpretera PHP
 *
 * @param int $errno
 *          Kod błędu
 * @param string $errstr
 *          Komunikat tekstowy
 * @param string $errfile
 *          Plik, w którym wystąpił błąd
 * @param int $errline
 *          Linia, w której wystąpił błąd
 * @param array $errcontext
 *          Lista zmiennych, które istniały, gdy powstał błąd
 */
function errorHandler ( $errno, $errstr, $errfile, $errline, $errcontext = NULL ) {

  echo '
  <html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <style>
  body {
  	font-family: Calibri;
  	font-size: 14px;
    line-height: 25px;
  }
  </style>
  </head>
  <body>
  <b>Błąd </b>
  <br><b>Kod:</b> ' . $errno . '
  <br><b>Komunikat:</b> ' . $errstr . '
  <br><b>Linia:</b> ' . $errline . '
  <br><b>Plik:</b> ' . $errfile . '
  <br><b>Zmienne:</b><br>
  ';

  pr ( $errcontext );

  echo '
  <br>
  <hr>
  <br>
  </body>
  </html>
  ';

  die;

}




?>